package reader;

import java.io.BufferedReader;


import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import ttspclass.TTSPData;
import utils.Intervention;
import utils.Team;
import utils.Technician;

public class SolutionReader {
	
	private List<List<Team>> teams = new ArrayList<List<Team>>();
	private List<String> s = new ArrayList<String>();
	
	private Team getTeamById(int k, int id) {
		for (Team t : teams.get(k)) {
			if (t.getId() == id) {
				return t;
			}
		}
		return null;
	}
	
	public List<String> getS() {
		return this.s;
	}
	
	public List<List<Team>> getTeams() {
		return teams;
	}

	public SolutionReader(String filename) {
		TTSPData data = new TTSPData(filename);
		this.tech_reader(filename, data);
		this.interv_reader(filename, data);
	}
	
	public SolutionReader(String filename, TTSPData data) {
		this.tech_reader(filename, data);
		this.interv_reader(filename, data);
	}
	
	public void add_team_on_day(Team team, int k) {
		teams.get(k).add(team);
	}
	
	public void create_day() {
		teams.add(new ArrayList<Team>());
	}
	
	private void tech_reader(String filename, TTSPData data) {
		filename += "/tech_teams";
		System.out.println("<read> Read the data from the technician teams file : " + filename);
		try {
			File file = new File(filename);
			BufferedReader buf = new BufferedReader(new FileReader(file)); 
			String str;
			int i = 0;
			int k = 0;
			int j;
		    int tmp;
		    int i_2;
			List<Technician> techs;
			Team t;
			while ((str = buf.readLine()) != null) {
				this.create_day();
				j = 0;
		        while (str.length() > i) {
		        	// je cherche la prochaine team
		        	while (str.length() > i && str.charAt(i) != '[') {
		                i++;
		            }
		            // Je rentre dans un crochet d une team
		        	
		            techs = new ArrayList<Technician>();
		            while (str.length() > i && str.charAt(i) != ']') {
		            	//Je cherche les techs dans le crochet
		                if (str.charAt(i) >= '0' && str.charAt(i) <= '9') {
		                	i_2 = i + 1;
		                	while (str.charAt(i_2) >= '0' && str.charAt(i_2) <= '9')
		                		i_2++;
		                    tmp = Integer.parseInt(str.substring(i, i_2));
		                    i += Math.log10(tmp);
		                    //j'ajoute un tech
		                    techs.add(data.getTechnicianById(tmp));
		                }
		                i++;
		            }
		            // j ai trouve tous les tech de la team, je crée leur team.
		            if (techs.size() != 0) {
		            	t = new Team(j, k, techs);
		            	this.add_team_on_day(t, k);
		            }
		            j++;
		        }
		        k++;
			}
			buf.close();
		}
		catch(Exception e)
		{  
			e.printStackTrace();  
		}
	}
	
	private void interv_reader(String filename, TTSPData data) {
		filename = filename + "/interv_dates";
		System.out.println("<read> Read the data from the intervention dates file : " + filename);
		try {
			String[] sol_tab;
			File file = new File(filename);
			BufferedReader buf = new BufferedReader(new FileReader(file)); 
			String str;
			Intervention inter;
			//this.solution = new TTSPSolution(); déjà créer au dessus donc inutile ?
			int id;
			int start;
			int day;
			int team;
			Team t;
			while ((str = buf.readLine()) != null && str.charAt(0) != '\n') {
				sol_tab = str.split(" ");
				id = Integer.parseInt(sol_tab[0]);
				day = Integer.parseInt(sol_tab[1]) - 1;
				start =Integer.parseInt(sol_tab[2]);
				team = Integer.parseInt(sol_tab[3]);
				inter = data.getInterventionById(id);
				if (inter != null)  {
					inter.setDay(day);
					inter.setStart(start);
					inter.setTeam(team);
				}
				t = this.getTeamById(day, team);
				if (t != null) {
					t.getIntervention().add(inter);
				}
				if (t == null && inter == null) {
					s.add("team numero : " + team + "doesn't exist");
					s.add("intervention numero : " + id + "doesn't exist");
				}
			}
			
			buf.close();
		}
		catch(Exception e)
		{  
			e.printStackTrace();  
		}
	}
}
