package reader;
import java.util.List;


import utils.Technician;

import java.util.ArrayList;
import java.io.*;

public class TechnicianReader {
	private List<Technician> techs = new ArrayList<Technician>();
	

	public TechnicianReader(String filename, int d_max)
	{
		this.reader(filename, d_max);
	}
	
	private void reader(String filename, int d_max)
	{
		try {
			System.out.println("<read> Read the data from the technicians file : " + filename);
			String[] tech_tab, dispo_tab;
			File file = new File(filename);
			BufferedReader buf = new BufferedReader(new FileReader(file));  
			String str, dispo;
			List<Integer> d;
			str = buf.readLine();
			int start = 0, end = 0;
			while ((str = buf.readLine()) != null)
			{
				d = new ArrayList<Integer>();

				for (int i = 0; i < str.length(); i++) {
					if (str.charAt(i) == '[')
						start = i;
					else if (str.charAt(i) == ']')
						end = i;
				}
				if (start + 1 != end - 1)
					dispo = str.substring(start + 2, end - 1);
				else
					dispo = null;
				str = str.substring(0, start -1);
				tech_tab = str.split(" ");
			
				if (dispo != null)
					dispo_tab = dispo.split(" ");
				else 
					dispo_tab = null;
				for (int i = 0; i < d_max; i++) {
					d.add(Integer.parseInt(tech_tab[i + 1]));
				}
				techs.add(new Technician(Integer.parseInt(tech_tab[0]), d, dispo_tab));
			}
			buf.close();
		}
		catch(Exception e)
		{  
			e.printStackTrace();  
		} 
	}
	
	public List<Technician> getTechnicians() {
		return techs;
	}
	
	
}
