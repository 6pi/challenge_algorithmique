package reader;
import java.io.*;


import java.util.List;

import utils.Intervention;
import utils.Instance;

import java.util.ArrayList;
import java.util.Arrays;

public class InterventionReader {
	private List<Intervention> intervs = new ArrayList<Intervention>();
	

	public InterventionReader(String filename, Instance instance)
	{
		this.reader(filename, instance);
	}
	
	private void reader(String filename, Instance instance)
	{
		try {
			System.out.println("<read> Read the data from the interventions file : " + filename);
			String[] inter_tab, preds_tab, d_tab;
			File file = new File(filename);
			BufferedReader buf = new BufferedReader(new FileReader(file));  
			String str, dispo;
			str = buf.readLine();
			int start = 0, end = 0;
			
			while ((str = buf.readLine()) != null)
			{

				for (int i = 0; i < str.length(); i++) {
					if (str.charAt(i) == '[')
						start = i;
					else if (str.charAt(i) == ']')
						end = i;
				}
				if (start + 1 != end - 1)
					dispo = str.substring(start + 2, end - 1);
				else
					dispo = null;
				
				str = str.substring(0, start -1) + str.substring(end + 1, str.length() - 1);
				inter_tab = str.split(" ");
			
				if (dispo != null)
					preds_tab = dispo.split(" ");
				else 
					preds_tab = null;
				
				d_tab = Arrays.copyOfRange(inter_tab, 4, 10);
				intervs.add(new Intervention(Integer.parseInt(inter_tab[0]), Integer.parseInt(inter_tab[1]),
						Integer.parseInt(inter_tab[2]), Integer.parseInt(inter_tab[3]), d_tab, preds_tab, instance));
			}
			buf.close();
		}
		catch(Exception e)  
		{  
			e.printStackTrace();  
		}
	}
	
	public List<Intervention> getInterventions() {
		return intervs;
	}
}
