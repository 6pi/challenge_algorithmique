package reader;
import java.io.*;

import utils.Instance;


public class InstanceReader {
	
	private Instance instance;
	
	public InstanceReader(String filename) {
		this.reader(filename);
		return;
	}
	
	private void reader(String filename) {
		try {
			System.out.println("<read> Read the data from the instance file : " + filename);
			String[] instance_tab;
			BufferedReader buf = new BufferedReader(new FileReader(filename));  
			String str;
			str = buf.readLine();
			str = buf.readLine();
			instance_tab = str.split(" ");
			this.instance = new Instance(instance_tab[0], Integer.parseInt(instance_tab[1]),
					Integer.parseInt(instance_tab[2]), Integer.parseInt(instance_tab[3]), Integer.parseInt(instance_tab[4]), Integer.parseInt(instance_tab[5]));
			buf.close();
		}
		catch(Exception e)
		{  
			e.printStackTrace();  
		} 
	}
	
	public Instance getInstance() {
		return this.instance;
	}
	
}
