package evaluator;

import ttspclass.*;
import utils.Intervention;

public class Evaluator {
	private int[] c = {0,0,0,0};
	private int cost = 0;
	private int id_max = -1;
	private int[] id = {-1,-1,-1};
	
	private int maxArray(int[] arr) {
		int max = 0;
		for (int i = 0; i < 3; i++) {
			if (max < c[i])
				max = c[i];
				id_max = id[i];
		}
		return max;
	}
	
	public void computeCost(TTSPData data) {
		int tmp;
		for (Intervention i : data.getIntervs()) {
			tmp = i.getDay() * 120 + i.getStart() + i.getTime();
			if (tmp > c[i.getPriority() - 1]) {
				c[i.getPriority() - 1] = tmp;
				id[i.getPriority() - 1] = i.getId();
			}
		}
		this.c[3] = maxArray(c);
		
		this.cost = 28 * c[0] + 14 * c[1] + 4 * c[2] + c[3];
	}
	
	public int[] getPriorityCosts() {
		return this.c;
	}
	
	public void display() {
		System.out.println("---------------------------------");
		System.out.println("--------- COMPUTE COST ----------");
		System.out.println("---------------------------------");
		System.out.println("Cost for interventions of priority 1 = " + c[0] * 28 + " (latest completion time = " + c[0] + " -> intervention #" + id[0] + ")");
		System.out.println("Cost for interventions of priority 2 = " + c[1] * 14 + " (latest completion time = " + c[1] + " -> intervention #" + id[1] +")");
		System.out.println("Cost for interventions of priority 3 = " + c[2] * 4 + " (latest completion time = " + c[2] + " -> intervention #" + id[2] +")");
		System.out.println("Schedule cost = " + c[3] + " (latest completion time = " + c[3] + " -> intervention #" + id_max + ")");
		System.out.println("-> TOTAL COST = " + cost);
		System.out.println("");
	}
	
	public int getCost() {
		return cost;
	}
	
	public Evaluator(TTSPData data) {
		this.computeCost(data);
	}

}
