package evaluator;

import ttspclass.TTSPSolution;
import ttspclass.TTSPData;

public class MainEvaluator {

	public static void main(String[] args) {
		if (args.length == 0)
		{
			System.out.println("You should provide exactly 1 program argument equal to the path to a folder containing \n" + 
					"    - intervention dates file (named \"interv_dates\")\n" + 
					"    - technician teams file (named \"tech_teams\")");
			return;
		}
		TTSPData data = new TTSPData(args[0]);
		TTSPSolution sol = new TTSPSolution(args[0].substring(1), data);
		sol.display(data);
		Evaluator ev = new Evaluator(data);
		ev.display();
	}
}
