package ttspclass;
import java.util.ArrayList;
import java.util.List;


import reader.InstanceReader;
import reader.InterventionReader;
import reader.TechnicianReader;
import utils.Instance;
import utils.Intervention;
import utils.Technician;

public class TTSPData {
	private List<Intervention> intervs;
	private List<Technician> techs;
	private Instance instance;
	
	public TTSPData(String filename){
		InstanceReader instR = new InstanceReader(filename + "/instance");
		this.instance = instR.getInstance();
		InterventionReader interR = new InterventionReader(filename + "/interv_list", this.instance);
		TechnicianReader techR = new TechnicianReader(filename + "/tech_list", instance.getDomains());
		this.intervs = interR.getInterventions();
		this.techs = techR.getTechnicians();
	}
	
	public List<Intervention> getIntervs() {
		return this.intervs;
	}
	
	
	public List<Technician> getTechs() {
		return this.techs;
	}
	
	public Instance getInstance() {
		return instance;
	}
	
	public Intervention getInterventionById(int id) {
		for (Intervention i : intervs) {
			if (i.getId() == id)
				return i;
		}
		return null;
	}
	
	public Technician getTechnicianById(int id) {
		for (Technician i : techs) {
			if (i.getId() == id) {
				return i;
			}
		}
		return null;
	}
	
	public int getBudget() {
		return this.instance.getBudget();
	}
	
	public boolean isPredsDone(int id) {
		Intervention inter = getInterventionById(id);
		Intervention tmp;
		if (inter.getPreds() == null)
			return true;
		for (int i : inter.getPreds())
		{
			tmp = getInterventionById(i);
			if (tmp.isDone() == false)
				return false;
		}
		return true;
	}
	
	public int isPredsOutsourced(int id) {
		Intervention inter = getInterventionById(id);
		Intervention tmp;
		for (int i : inter.getPreds()) {
			tmp = getInterventionById(i);
			if (tmp.getOutsourced()) {
				return i;
			}
		}
		return (-1);
	}
	
	public void display() {
		System.out.println("///////////// Instance " + instance.getName() + " ////////////");
		System.out.println("#Interventions = " + intervs.size());
		System.out.println("#Technicians = " + techs.size());
		System.out.println("#Domains / #Levels = " + instance.getDomains() + " / " + instance.getLevel());
		System.out.println("Outsourcing budget = " + instance.getBudget());
		System.out.println("");

		System.out.println("----------------------------------");
		System.out.println("--------- INTERVENTIONS ----------");
		System.out.println("----------------------------------");
		
		for (Intervention i : intervs) {
			System.out.println(" -> Interv #" + (i.getId()));
			i.display(instance);
		}
		System.out.println();
		System.out.println("----------------------------------");
		System.out.println("---------- TECHNICIANS -----------");
		System.out.println("----------------------------------");
		
		for (Technician i : techs) {
			System.out.println("-> Tech #" + (i.getId()));
			i.display(instance);
		}
		System.out.println("//////////////////////////////////");
	}
}
