package ttspclass;
import java.util.List;


import utils.Intervention;
import utils.Team;
import utils.Technician;

import reader.SolutionReader;



public class TTSPSolution {
	private List<List<Team>> teams;
	private List<String> s;
	
	public TTSPSolution(String file) {
		SolutionReader sr = new SolutionReader(file);
		teams = sr.getTeams();
		s = sr.getS();
	}
	
	public TTSPSolution(String file, TTSPData data) {
		SolutionReader sr = new SolutionReader(file, data);
		teams = sr.getTeams();
		s = sr.getS();
		return;
	}
	
	public TTSPSolution() {
		return;
	}
	
	public List<List<Team>> getTeams() {
		return teams;
	}
	
	public List<String> getS() {
		return this.s;
	}

	public void add_day(List<Team> teams) {
		this.teams.add(teams);
	}

	
	public void display(TTSPData data) {
		System.out.println("///////////// Solution ////////////");
		System.out.println("----------------------------------");
		System.out.println("--------- INTERVENTION SCHEDULE ----------");
		System.out.println("----------------------------------");
		for (Intervention i : data.getIntervs()) {
			System.out.println("#" + i.getId() + " -> day " + (i.getDay() + 1) + " starts at time " + i.getStart() + "\t / team # " + i.getTeam() + "\t outsourced = " + (i.getOutsourced() ? "1" : "0"));
		}
		System.out.println();
		System.out.println("----------------------------------");
		System.out.println("---------- TECHNICIAN TEAMS -----------");
		System.out.println("----------------------------------");
		int jour = 1;
		for (List<Team> tmp : teams) {
			System.out.println("Teams on day " + jour);
			for (Team t : tmp) {
				System.out.print("#" + t.getId() + " ->");
				for (Technician tech : t.getTechnician()) {
					System.out.print(" " + tech.getId());
				}
				System.out.println(" ");
			}
			jour = jour + 1;
		}
		System.out.println("//////////////////////////////////");
		System.out.println("");
		System.out.println("");
	}
}

