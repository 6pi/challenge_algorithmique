package mainReader;

import java.io.File;

import ttspclass.TTSPSolution;
import ttspclass.TTSPData;

public class MainSolutionReader {
	
	public static void main(String[] args)
	{
		if (args.length == 0) {
			System.out.println("You should provide exactly 1 program argument equal to the path to a folder containing \n" + 
					"    - instance file (named \"instance\")\n" + 
					"    - interventions file (named \"interv_list\")\n" + 
					"    - technicians file (named \"tech_list\")\n"); 
			return;
		}
		TTSPData data;
		TTSPSolution sol;
		if (args[0].charAt(0) == '/') {
			data = new TTSPData(args[0].substring(1));
			sol = new TTSPSolution(args[0].substring(1), data);
		}
		else {
			File directory = new File("./");
			data = new TTSPData(directory.getAbsolutePath() + "/" + args[0]);
			sol = new TTSPSolution(directory.getAbsolutePath() + "/" + args[0], data);
		}
		sol.display(data);
		return;
	}

}
