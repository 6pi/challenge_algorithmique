package Cplex;
import java.io.File;
import ttspclass.TTSPData;
import Cplex.CplexSolver;

public class MainSolver {
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("You should provide exactly 1 program argument equal to the path to a folder containing \n" + 
					"    - instance file (named \"instance\")\n" + 
					"    - interventions file (named \"interv_list\")\n" + 
					"    - technicians file (named \"tech_list\")\n"); 
			return;
		}
		TTSPData data;
		String s;
		if (args[0].charAt(0) == '/') {
			s = args[0];
		}
		else {
			File directory = new File(".");
			s = directory.getAbsolutePath() + "/" + args[0];
		}
		data = new TTSPData(s.substring(1));
		data.display();
		CplexSolver solver = new CplexSolver();
		solver.solve(data);
		return;
	}

}
