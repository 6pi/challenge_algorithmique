package Cplex;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ttspclass.*;

import java.util.List;

import evaluator.Evaluator;


public class CplexSolver {

	TTSPData data;


	public void solve(TTSPData data) {
		try {
			IloCplex cplex = new IloCplex();

			int n = data.getInstance().getN_interv();
			int m = data.getInstance().getN_tech();
			Evaluator ev = new Evaluator(data);
			IloLinearNumExpr ct = cplex.linearNumExpr();;

			//Create Variables
		
			//Variable (18)
			IloNumVar[] b = cplex.numVarArray(n, 0.0, 120*n);
			for (int i = 0; i < n; ++i) {
				b[i].setName("b(" + i + ")");
			}
			
			//Variable (19)
			IloNumVar[][][] x = new IloNumVar[m][][];
			for (int t = 0; t < m; ++t) {
				x[t] = new IloNumVar[m][];
				for (int e = 0; e < m; ++e) {
					x[t][e] = cplex.boolVarArray(n);
					for (int k = 0; k < n; ++k) {
						x[t][e][k].setName("x(" + t + "," + e +"," + k + ")");
					}
				}
			}
			
			//Variable (20)
			IloNumVar[][][] y = new IloNumVar[m][][];
			for (int e = 0; e < m; ++e) {
				y[e] = new IloNumVar[n][];
				for (int i = 0; i < n; ++i) {
					y[e][i] = cplex.boolVarArray(n);
					for (int k = 0; k < n; ++k) {
						y[e][i][k].setName("y(" + e + "," + i +"," + k + ")");
					}
				}
			}
			
			// Variables (21)
			IloNumVar[] z = cplex.boolVarArray(n);
			for (int i = 0; i < n; ++i) {
				z[i].setName("z(" + i + ")");
			}
			
			//Definition des fonctions
			//Fonction u(i,j) vaut 1 si i est exécutée avant j
			IloNumVar[][] u = new IloNumVar[n][];
			for (int i = 1; i < n; ++i) {
				u[i] = cplex.boolVarArray(n);
				for (int j = 1; j < i; ++j) {
					u[i][j].setName("u(" + i + "," + j + ")");
				}
			}
			//Fonction s'(t,d,l), vaut 1 si s(t,d)>=l
			int dom = data.getInstance().getDomains();
			int lev = data.getInstance().getLevel();
			Integer[][][] S = new Integer[m][dom][lev];
			for (int t = 0; t < m; ++t) {
				for (int d = 0; d < dom; ++d) { 
					for (int l = 0; l < lev; ++l) { 
						if ( data.getTechnicianById(t) != null && data.getTechnicianById(t).getSkills().get(d) >= l) {
							S[t][d][l] = 1;
						}else {
							S[t][d][l] = 0;
						}
					}
				}
			}
			//Fonction v(t,k) vaut 1 si k n'appartient pas à ^K_t
			Integer[][] v = new Integer[m][n];
			for (int t = 0; t < m; ++t) {
				for (int k = 0; k < n; ++k) {
					if( data.getTechnicianById(t) != null && data.getTechnicianById(t).isAvailable(k) ) {
						v[t][k] = 1;
					}else {
						v[t][k] = 0;
					}
				}
			}
			
			
			//Variables de decision
			IloNumVar[] c = cplex.numVarArray(4, 0.0, 120*n);
			for (int i = 0; i < 4; ++i) {
				c[i].setName("c(" + i + ")");
			}
			
			//Creation de la fontion objectif
			IloLinearNumExpr obj = cplex.linearNumExpr();
			int[] factors = {28, 14, 4, 1};
			for (int i = 0; i < 4; i++) {
				obj.addTerm((double)factors[i], c[i]);
			}
			cplex.addMinimize(obj);
			

			//Contraintes (2)(3)(4)(5)
			for (int i = 1; i < n; ++i) {
				if( data.getInterventionById(i) != null ) {
					int prio = data.getInterventionById(i).getPriority();
					if( prio > 0 && prio < 4) {
						ct.clear();
						ct.addTerm(1.0, b[i]);
						ct.addTerm(-1.0, c[prio -1]);
						cplex.addLe(ct, data.getInterventionById(i).getTime());
					}
				}
			}
			for (int i = 1; i < 3; ++i) {
				cplex.addLe( c[i], c[3]);
			}
			
			
			//Creation des contraintes
			//Contrainte (6)
			for (int k = 0; k < n; ++k) {
				for (int t = 0; t < m; ++t) {
					ct.clear();
					for (int e = 1; e < m; ++e) {
						ct.addTerm(1.0, x[t][e][k]);
					}
					cplex.addLe(ct, 1.0);
				}
			}
			
			//Contrainte (7)
			for (int i = 0; i < n; i++) {
				if( data.getInterventionById(i) != null && data.getInterventionById(i).getPreds() != null ) {
					for (int j : data.getInterventionById(i).getPreds()) { //pour j dans Pred(i)
						ct.clear();
						ct.addTerm(1.0, b[j]);
						ct.addTerm(-120*n, z[i]);
						ct.addTerm(-1.0, b[i]);
						cplex.addLe(ct, -1.0 * data.getInterventionById(j).getTime());
					}
				}
			}
			
			//Contrainte (8)
			for (int i = 0; i < n; i++) {
				if( data.getInterventionById(i) != null && data.getInterventionById(i).getPreds() != null ) {
					for (int j : data.getInterventionById(i).getPreds()) { //pour j dans Pred(i)
						ct.clear();
						ct.addTerm(1.0, z[j]);
						ct.addTerm(-1.0, z[i]);
						cplex.addLe(ct, 0.0);
					}
				}
			}
			
			//Contrainte (9)
			ct.clear();
			for (int i = 0; i < n; i++) {
				if( data.getInterventionById(i) != null) {
					ct.addTerm(data.getInterventionById(i).getCost(), z[i]);
				}
			}
			cplex.addLe(ct, data.getBudget());
			
			//Contrainte (10)
			for (int e = 1; e < m; ++e) {
				for (int i = 0; i < n; ++i) {
					for (int k = 0; k < n; ++k) {
						for (int d = 0; d < dom; ++d) {
							for (int l = 0; l < lev; ++l) {
								if( data.getInterventionById(i) != null ) {
									ct.clear();
									List<List<Integer>> r = data.getInterventionById(i).getSkills();
									ct.addTerm( r.get(d).get(l), y[e][i][k]); 
									for (int t = 0; t < m; ++t) {
										ct.addTerm(-1.0*S[t][d][l], x[t][e][k]);
									}
									cplex.addLe(ct, 0.0);
								}
							}
						}
					}
				}
			}
			
			//Contrainte (11)
			for (int i = 0; i < n; ++i) {
				ct.clear();
				for (int e = 1; e < m; ++e) {
					for (int k = 0; k < n; ++k) {
						ct.addTerm(1.0, y[e][i][k]);
					}
				}
				ct.addTerm(1.0, z[i]);
				cplex.addEq(ct, 1.0);
			}

			//Contrainte (12)
			for (int i = 0; i < n; i++) {
				for (int k = 0; k < n; k++) {
					ct.clear();
					for (int e = 1; e < m; e++) {
						ct.addTerm(120*k, y[e][i][k]);
					}
					ct.addTerm(-1.0, b[i]);
					cplex.addLe(ct, 0.0);
				}
			}
			
			//Contrainte (13)
			for (int i = 0; i < n; i++) {
				if( data.getInterventionById(i) != null ) {
					for (int k = 0; k < n; k++) {
						ct.clear();
						ct.addTerm(1.0, b[i]);
						for (int e = 1; e < m; e++) {
							ct.addTerm(-120*(k+1), y[e][i][k]);
							ct.addTerm(120*n, y[e][i][k]);
						}
						cplex.addLe(ct, 120*n - data.getInterventionById(i).getTime());
					}
				}
			}
			
			//Contrainte (14)
			for (int t = 0; t < m; ++t) {
				for (int e = 0; e < m; ++e) {
					for (int k = 0; k < n; ++k) {
						ct.clear();
						ct.addTerm(1.0, x[t][e][k]);
						cplex.addLe(ct, v[t][k]);
					}
				}
			}
			
			//Contrainte (15)
			for (int e = 1; e < m; ++e) {
				for (int k = 0; k < n; ++k) {
					for (int i = 0; i < n; ++i) {
						for (int j = 0; j < i; ++j) {
							ct.clear();
							ct.addTerm(1.0, u[i][j]);
							//ct.addTerm(1.0, u[j][i]);
							ct.addTerm(-0.5, y[e][i][k]);
							ct.addTerm(-0.5, y[e][j][k]);
							cplex.addLe(ct, 0.0);
						}
					}
				}
			}
			
			//Contrainte (16)
			for (int e = 1; e < m; ++e) {
				for (int k = 0; k < n; ++k) {
					for (int i = 0; i < n; ++i) {
						for (int j = 0; j < i; ++j) {
							ct.clear();
							ct.addTerm(1.0, y[e][i][k]);
							ct.addTerm(1.0, y[e][j][k]);
							ct.addTerm(-1.0, u[i][j]);
							//ct.addTerm(-1.0, u[j][i]);
							cplex.addLe(ct, 1.0);
						}
					}
				}
			}
			
			//Contrainte (17)
			for (int i = 0; i < n; ++i) {
				if( data.getInterventionById(i) != null ) {
					for (int j = 0; j < i; ++j) {
						ct.clear();
						ct.addTerm(1.0, b[i]);
						ct.addTerm(-1.0, b[j]);
						ct.addTerm(120*n, u[i][j]);
						cplex.addLe(ct, 120*n - data.getInterventionById(i).getTime());
					}
				}
			}
			
			
			cplex.exportModel("model.lp");
			cplex.setParam(IloCplex.IntParam.TimeLimit, 600);
			cplex.setParam(IloCplex.IntParam.Threads, 1);

			
			double startTime = cplex.getCplexTime();
			if (cplex.solve()) {
				double endTime = cplex.getCplexTime();
				System.out.println("Succes! (Status: " + cplex.getStatus() + ")"); // < prints the solver status
				System.out.println("Runtime : " + Math.round((endTime - startTime) * 1000) / 1000.0 + " seconds");
				System.out.println("Objective value = " + cplex.getObjValue());
				
				//Construction d'une solution
				System.out.println("Les interventions sous-traitées :");
				for(int i = 0; i < n; i++) {
					if(cplex.getValue(z[i]) == 1) {
						System.out.println("Id de l'intervention = " + i);
					}
				}
				for(int k = 0; k < n; k++) {
					System.out.println("Jour " + (k+1) + " :");
					for(int e = 0; e < m; e++) {
						System.out.println("L'équipe " + e + " est composée des techniciens :");
						for(int t = 0; t < m; t++) {
							if( cplex.getValue(x[t][e][k]) == 1) {
								System.out.println("Id du technicien = " + t);
							}
						}
						System.out.println("et réalise les interventions :");
						for(int i = 0; i < n; i++) {
							if( cplex.getValue(y[e][i][k]) == 1) {
								System.out.println("Id de l'intervention = " + i);
							}
						}
					}
				}
				for(int i = 0; i < n; i++) {
					System.out.println("L'intervention " + i + "commence à " + cplex.getValue(b[i]));
				}
				
			} else {
				System.err.println("Fail! (Status: " + cplex.getStatus() + ")");

			}
			
			cplex.end();
		} catch (IloException e) {
			System.err.print("Cplex exception");
			e.printStackTrace();
		}


	}

}
