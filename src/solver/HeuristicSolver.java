package solver;

import java.util.Collections;
import ttspclass.TTSPData;
import ttspclass.TTSPSolution;
import utils.Intervention;
import java.util.ArrayList;
import java.util.List;

public class HeuristicSolver {
	private int checkIntervs(TTSPData data) {
		int budget = data.getInstance().getBudget();
		int n = 0;
		for (Intervention i : data.getIntervs()) {
			if (budget - i.getCost() >= 0) {
				budget -= i.getCost();
				if (i.getTime() > 120) {
					i.setOurtsourced();
					n++;
				}
				/*
				 * if (check des techs)
				 */
			}
			else {
				System.err.println("Error not Enough budget.");
				return -1;
			}
			for (int pred : i.getPreds()) {
				Intervention tmp = data.getInterventionById(pred);
				if (budget - tmp.getCost() >= 0) {
					budget -= tmp.getCost();
					tmp.setOurtsourced();
					n++;
				}
				else {
					System.err.println("Error not Enough budget.");
					return -1;
				}
			}
		}
		data.getInstance().refresh_budget = budget;
		return n;
	}
	
	public TTSPSolution Solve(TTSPData data) {
		TTSPSolution sol = new TTSPSolution();
		Collections.sort(data.getIntervs());
		int n = this.checkIntervs(data);
		
		int end = data.getInstance().getN_interv() - n;
		while (end != 0) {
			
		}
		return sol;
	}
}
