package solver;

import ttspclass.TTSPData;
import ttspclass.TTSPSolution;
import java.io.File;

public class MainSolver {
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("You should provide exactly 1 program argument equal to the path to a folder containing \n" + 
					"    - instance file (named \"instance\")\n" + 
					"    - interventions file (named \"interv_list\")\n" + 
					"    - technician teams file (named \"tech_teams\")");
			return;
		}
		TTSPData data;
		String s;
		if (args[0].charAt(0) == '/') {
			s = args[0];
		}
		else {
			File directory = new File(".");
			s = directory.getAbsolutePath() + "/" + args[0];
		}
		data = new TTSPData(s);
		TTSPSolution sol = new HeuristicSolver().Solve(data);
		
		return;
	}

}
