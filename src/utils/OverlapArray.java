package utils;

public class OverlapArray {
	public int id;
	public int start;
	public int end;

	public OverlapArray(int id, int start, int end) {
		this.id = id;
		this.start = start;
		this.end = end;
	}
}
