package utils;



public class Instance {
	private String name;
	private int domains;
	private int n_tech;
	private int n_interv;
	private int budget;
	private int level;
	public int refresh_budget;

	public Instance(String name, int domains, int level, int n_tech, int n_interv, int budget) {
		super();
		this.name = name;
		this.domains = domains;
		//add one to handle the level 0
		this.level = level + 1;
		this.n_tech = n_tech;
		this.n_interv = n_interv;
		this.budget = budget;
		this.refresh_budget = budget;
	}
	public int getDomains() {
		return domains;
	}
	public int getN_tech() {
		return n_tech;
	}
	public int getN_interv() {
		return n_interv;
	}
	public int getBudget() {
		return budget;
	}
	
	public int getLevel() {
		return level;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean subBudget(Intervention inter) {
		if (budget - inter.getCost() < 0)
			return false;
		budget -= inter.getCost();
		return true;
	}
	
}
