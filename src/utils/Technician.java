package utils;
import java.util.ArrayList;
import java.util.List;

public class Technician {
	private List<Integer> skills;
	private List<Integer> k = new ArrayList<Integer>();
	private int id;
	
	public Technician(int id, List<Integer> d, String[] k) {
		this.id = id;
		if (k != null)
			this.createDispoTab(k);
		else
			this.k = null;
		this.skills = d;
	}
	
	private void createDispoTab(String[] k)
	{
		for (String tmp : k)
		{
			this.k.add(Integer.parseInt(tmp));
		}
		return;
	}
	
	public boolean isAvailable(int day)
	{
		if (k == null)
			return true;
		for (int i : this.k)
		{
			if (i == day)
				return false;
		}
		return true;
	}
	
	public int getId()
	{
		return this.id;
	}

	public int s(int d)
	{
		return this.skills.get(d);
	}
	
	public List<Integer> getSkills()
	{
		return skills;
	}
	
	public void display(Instance instance) {
		System.out.print("Skills (mastered level per domain) ->");
		for (int i = 0; i < instance.getDomains(); i++) {
			System.out.print(" " + skills.get(i));
		}
		System.out.print(" ");
		for (int i = 0; i < instance.getDomains(); i++) {
			System.out.println();
			System.out.print("Skills for domain " + (i+1) + " -> ");
			for (int j = 0; j < instance.getLevel(); j++) {
				if (this.skills.get(i) < j) {
					System.out.print("0 ");
				}
				else
					System.out.print("1 ");
			}
		}
		System.out.println("");
		System.out.print("Not available on day -> ");
		if (k != null) {
		for (int i : k) {
				System.out.print(i + " ");
			}
		}
		System.out.println("");
	}
	
}
