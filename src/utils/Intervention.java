package utils;

import java.util.List;
import java.util.ArrayList;

public class Intervention implements Comparable<Intervention> {
	private int id;
	private int time;
	private int priority;
	private int cost;
	private List<List<Integer>> skills;
	private List<Integer> preds;
	private boolean done = false;
	private int day;
	private int start;
	private int team;
	private boolean outsourced = false;
	private int tech_needed = 0;
	private int weight;
	
	public Intervention(int id, int time, int priority, int cost, String[] d, String[] preds, Instance instance) {
		this.id = id;
		this.time = time;
		this.priority = priority;
		this.cost = cost;

		this.skills = convertSkills(d, instance);
		
		if (preds != null)
			this.preds = convertPreds(preds);
		else
			this.preds = null;
		int j;
		for (List<Integer> list :  this.skills) {
			j = 1;
			for (int i : list) {
				weight += i * j;
				j++;
			}
		}
	}
	
	private List<Integer> convertPreds(String []tab) {
		List<Integer> ret = new ArrayList<Integer>();
		for (int i = 0; i < tab.length; i++) {
			ret.add(Integer.parseInt(tab[i]));
		}
		return ret;
	}
	
	private List<List<Integer>> convertSkills(String[] tab, Instance instance) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		ArrayList<Integer> tmp;
		for (int i = 0; i < instance.getDomains(); i++) {
			tmp = new ArrayList<Integer>();
			tmp.add(0); //ajout du level 0
			for (int j = 0; j < instance.getLevel() - 1; j++) {
				tmp.add(Integer.parseInt(tab[i * (instance.getLevel() -1) + j]));
			}
			ret.add(tmp);
		}
		for (List<Integer> main : ret) {
			for (int i : main) {
				tech_needed += i;
			}
		}
		return ret;
	}
	
	public int getTechNeeded() {
		return tech_needed;
	}
	
	public boolean InterventionCanBeDone(int start) {
		start = start % 120;
		if (start + this.time > 120)
				return false;
		return true;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public void setOurtsourced() {
		this.outsourced = true;
	}
	
	public void setInterventionToDone() {
		this.done = true;
	}
	
	public boolean isDone() {
		return this.done;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getCost() {
		return this.cost;
	}
	
	public List<Integer> getPreds() {
		return this.preds;
	}
	
	public int getPriority() {
		return this.priority;
	}
	
	public int getDay() {
		return this.day;
	}
	
	public int getTime() {
		return this.time;
	}
	
	public void setStart(int start) {
		this.start = start;
	}
	
	public void setTime(int time) {
		this.time = time;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public void setTeam(int team) {
		this.team = team;
	}
	
	public boolean getOutsourced() {
		return outsourced;
	}
	
	public int getTeam() {
		return this.team;
	}
	
	public int getWeight() {
		return this.weight;
	}
	
	public List<List<Integer>> getSkills() {
		return skills;
	}
	
	@Override
	public int compareTo(Intervention i) {
		if (this.getPriority() > i.getPriority())
			return 1;
		if (this.getPriority() == i.getPriority()) {
			if (this.getWeight() > i.getWeight())
				return 1;
			else if (this.getWeight() == i.getWeight())
				return 0;
		}
		return -1;
	}

	public void display_final() {
		System.out.println(id + " " + day + " " + start + " " + team);
	}
	
	public void display(Instance instance) {
		System.out.println("Time =  " + this.time + " Priority = " + this.priority + " Cost = " + this.cost);
		for (int i = 0; i < instance.getDomains(); i++) {
			System.out.print("Number of technicians required for domain " + (i+1) + " -> ");
			for (int j = 0; j < instance.getLevel(); j++) {
				System.out.print(skills.get(i).get(j) + " ");
			}
			System.out.println("");
		}
		System.out.print("Predecessors = ");
		if (preds != null) {
			for (int i : preds) {
				System.out.print(i + " ");
			}
		}
		System.out.println("");
	}
}
