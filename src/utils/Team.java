package utils;
import java.util.List;
import java.util.ArrayList;

public class Team {
	private List<Technician> techs;
	private List<Intervention> intervs;
	private int day;
	private int id;
	
	public Team(int id, int k, List<Technician> techs, List<Intervention> intervs) {
		this.id = id;
		this.day = k;
		this.techs = techs;
		this.intervs = intervs;
	}
	
	public Team(int id, int k, List<Technician> techs) {
		this.id = id;
		this.day = k;
		this.techs = techs;
		this.intervs = new ArrayList<Intervention>();
	}
	
	public List<Intervention> getIntervention() {
		return intervs;
	}
	
	public int getDay() {
		return this.day;
	}
	
	public int getId() {
		return this.id;
	}
	
	public List<Technician> getTechnician() {
		return techs;
	}
	
	public String toString() {
		String s = "";
		for (Technician tech : techs) {
			s += tech.getId() + " ";
		}
		return s;
	}
	
	public void displayTechs() {
		for (Technician tech : techs) {
			System.out.print(tech.getId() + " ");
		}
		System.out.println("");
	}

	public void displayIntervs() {
		for (Intervention interv : intervs) {
			System.out.println(interv.getId());
		}
	}
}
