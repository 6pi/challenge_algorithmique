package checker;

import ttspclass.TTSPData;
import ttspclass.TTSPSolution;

import java.io.File;

public class MainChecker {
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("You should provide exactly 1 program argument equal to the path to a folder containing \n" + 
					"    - instance file (named \"instance\")\n" + 
					"    - interventions file (named \"interv_list\")\n" + 
					"    - technicians file (named \"tech_list\")\n" + 
					"    - intervention dates file (named \"interv_dates\")\n" + 
					"    - technician teams file (named \"tech_teams\")");
			return;
		}
		TTSPData data;
		TTSPSolution sol;
		String s;
		if (args[0].charAt(0) == '/') {
			s = args[0];
		}
		else {
			File directory = new File(".");
			s = directory.getAbsolutePath() + "/" + args[0];
		}
		data = new TTSPData(s);
		sol = new TTSPSolution(s.substring(1), data);
		SolutionChecker sc = new SolutionChecker(data, sol);
		sc.displayErrors();
		return;
	}

}
