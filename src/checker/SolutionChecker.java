package checker;

import java.util.List;
import java.util.ArrayList;

import ttspclass.TTSPData;
import ttspclass.TTSPSolution;

import utils.Intervention;
import utils.Team;
import utils.Technician;
import utils.OverlapArray;


public class SolutionChecker {
	private int b = 0;
	private List<String> errors;
	private int feasible = 1;
		
	public SolutionChecker(TTSPData data, TTSPSolution solution) {
		this.errors = solution.getS();
		this.checker(data, solution);
	}
	
	
	//=============================== TEAM CHECKS ===============================================
	
	private void t_in_list(List<Technician> techs, Technician t, int k) {
		for (Technician tmp : techs) {
			if (t == tmp) {
				errors.add("Technician #" + t.getId() + "is use on two Intervention on day " + k);
				return;
			}
		}
	}
	
	private void tech_exist(Technician t, List<Technician> techs, int day) {
		for (Technician tmp : techs) {
			if (t.getId() == tmp.getId()) {
				return;
			}
		}
		errors.add("Technician #" + t.getId() + " doesn't exist.");
	}
	
	private void isTechUnavailable(Technician tech, int k, int team_id) {
		if (tech.isAvailable(k) == false) {
			errors.add("Technician #" + tech.getId() + " is assigned on day " + k + " to team " 
					+ team_id + " but it should be assigned to team 0 since he/she is unavalaible");
		}
	}
	
	private boolean techIsInList(List<Technician> L, int id) {
		for (Technician tmp : L) {
			if (tmp.getId() == id)
				return true;
		}
		return false;
	}
	
	private void chekAllTechniciansWork(TTSPData data, List<Team> teams) {
		boolean in;
		int day;
		if (teams != null && teams.isEmpty() == false)
			day = teams.get(0).getDay();
		else
			return;
		for (Technician main_tech : data.getTechs()) {
			in = false;
			for (Team t : teams) {
				if (techIsInList(t.getTechnician(), main_tech.getId()) == true) {
					in = true;
					break;
				}
			}
			if (in == false) {
					errors.add("Technician #" + main_tech.getId() + " is not assigned to a team on day " + (day + 1));
				}
		}
	}
	
	
	//=============================== OVERLAP CHECKS ===============================================
	
	private OverlapArray createOverlapArray(Intervention inter) {
		return (new OverlapArray(inter.getId(), inter.getStart(), inter.getStart() + inter.getTime()));
	}
	
	private void areInterventionOverlapping(List<OverlapArray> overlap, int team_id) {
		int i = 0;
		int j = 0;
		OverlapArray main;
		OverlapArray test;
		while (i < overlap.size()) {
			main = overlap.get(i);
			j = i + 1;
			while (j < overlap.size()) {
				test = overlap.get(j);
				if ((test.start <= main.start && main.start < test.end) || (test.start < main.end && main.end <= test.end)) {
					errors.add("Intervention #" + main.id + " and #" + test.id +  " overlap on day 1 and are executed by the same team #" + team_id 
							+ " ([" + main.start + "," + main.end+ "] and [" + test.start + "," + test.end+ "])");
				}
				j++;
			}
			i++;
		}
	}
	
	//=============================== INTER CHECKS ===============================================
	
	private void interventionoverlapTwoDays(Intervention inter) {
		if (inter.InterventionCanBeDone(inter.getStart()) == false) {
			errors.add("Intervention #" + inter.getId() + " overlap on day : " + inter.getDay() + " and " + (inter.getDay() + 1));
		}
	}
	
	private void isInterventionPredsDone(TTSPData data, int id) {
		System.out.println(id);
		if (data.isPredsDone(id) == false) {
			errors.add("Predecessor of Intervention #" + id + " is not done.");
		}
	}

	private void isInterventionOutsourcedPredsDone(TTSPData data, int id) {
		int tmp = data.isPredsOutsourced(id);
		if (tmp != -1) {
			errors.add("Intervention # " + id + " is scheduled whereas its predecessor 2 is outsourced (the outsourcing of intervention # " +
					tmp + " implies the outsourcing of intervention #" + id + ")");
		}
	}
	
	//============================== TEAM SKILLS CHECK ============================================
	
	private int getTeamLevel(Team team, int domain) {
		int max = 0;
		for (Technician tech : team.getTechnician()) {
			if (tech.s(domain) > max)
				max = tech.s(domain);
		}
		return max;
	}
	
	private void checkSkills(Intervention inter, Team team) {
		List<List<Integer>> tmp = new ArrayList<List<Integer>>(inter.getSkills());
		int domain;
		for (Technician tech : team.getTechnician()) {
			domain = 0;
			for (int i: tech.getSkills()) {
				for (int j = i; j >= 0; j--) {
					if (tmp.get(domain).get(j) > 0) {
						tmp.get(domain).set(j, tmp.get(domain).get(j) -1);
					}
				}
				domain++;
			}
		}
		domain = 0;
		int l;
		int team_level;
		for (List<Integer> domains : tmp) {
			l = 0;
			for (int level : domains) {
				if (level != 0) {
					team_level = getTeamLevel(team, domain);
					errors.add("Team #" + team.getId() + " ( " + team.toString() + ") is not enough skilled to perform intervention " +
							inter.getId() + " (domain " + (domain + 1) + " / level " + team_level + " : " + team_level + "<" + l + ")");
				}
				l++;
			}
			domain++;
			
		}
		return;
	}
	
	//=============================== MAIN CHECKER ===============================================
	private void checker(TTSPData data, TTSPSolution solution) {
		int id;
		List<OverlapArray> overlap = new ArrayList<OverlapArray>();
		List<Technician> techs = new ArrayList<Technician>();
		for (List<Team> teams : solution.getTeams()) {
			for (Team t : teams) {
				for (Intervention inter : t.getIntervention()) {
					id = inter.getId();
					overlap.add(this.createOverlapArray(inter));

					if (inter.isDone()) {
						errors.add("Intervention done more than one time");
					}
					inter.setInterventionToDone();
					
					this.interventionoverlapTwoDays(inter);
					
					//Check Preds
					
					if (inter.getPreds() != null)
						this.isInterventionPredsDone(data, id);
					
					this.checkSkills(inter, t);
				}
				if (t.getId() != 0) {
					this.areInterventionOverlapping(overlap, t.getId());
					overlap.clear();
				}
				
				
				//Verifie que les techniciens ne soient pas dans deux equipes en meme temps et existent
				for (Technician tech : t.getTechnician()) {
					if (t.getId() != 0)
						this.isTechUnavailable(tech, t.getDay() + 1, t.getId());
					this.t_in_list(techs, tech, t.getDay());
					this.tech_exist(tech, data.getTechs(), t.getDay());
					techs.add(tech);
				}
				techs.clear();
			}
			this.chekAllTechniciansWork(data, teams);
		}
		//Add all errors generate by the Skill Checker
	
		String budget = "";
		//Check outsourced budget
		for (Intervention inter : data.getIntervs()) {
			if (inter.isDone() == false)
			{
				//check outsourced preds
				if (inter.getPreds() != null)
					this.isInterventionOutsourcedPredsDone(data, inter.getId());
				b += inter.getCost();
			}
			if (b > data.getBudget())
				budget += inter.getId() + " ";
		}
		if (b > data.getBudget())
				errors.add("The total cost of all the outsourced interventions is larger than the outsourcing budget : " + 
						b + " > " + data.getBudget() + " (outsourced interventions : " + budget + ")");
		if (errors.size() != 0) {
			feasible = 0;
		}
	}
	
	public void displayErrors() {
		System.out.println("---------------------------------");
		System.out.println("------- CHECK CONSTRAINTS -------");
		System.out.println("---------------------------------");
		for (String i : this.errors) {
			System.out.println("[issue] " + i);
		}
		System.out.println("-> FEASIBLE = " + feasible + "  (0=false/1=true)");
		System.out.println("");
	}
	
	public void displaySummary(int cost) {
		System.out.println("---------------------------------");
		System.out.println("----------- SUMMARY ------------");
		System.out.println("---------------------------------");
		System.out.println("- Feasible = " + feasible  + "  (0=false/1=true)\t");
		System.out.println("- Cost = " + cost);
	}
	

	
}
