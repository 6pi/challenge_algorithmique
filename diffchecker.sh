#!/bin/bash

for out in $(ls solutions)
do
	./checkerTTSP solutions/$out > .buff_checkerTTSP
	java -jar checker.jar solutions/$out > .buff_mychecker
	diff .buff_checkerTTSP .buff_mychecker > result/$out
done
rm .buff_checkerTTSP .buff_mychecker
