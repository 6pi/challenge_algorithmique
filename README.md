<h1> Challenge algorithmique </h1>
<h2>Présentation</h2>


<h2> Utilisation <h2>
<h4>Résolution</h4>
Exécutez la class MainInstanceReader avec comme argument le dossier contenant 3 fichiers : instance, interv_list, tech_list.

<h4>Lecture de Solution</h4>
Exécutez la class MainSolutionReader avec comme argument le dossier contenant 5 fichiers : instance, interv_list, tech_list, tech_teams, interv_dates.

<h4>Evaluateur de Solution</h4>
Exécutez la classe MainEvaluator avec le chemin d'un dossier contenant une instance et sa solution.

<h4>Checker de Solution</h4>
Exécutez la class MainChecker avec le chemin d'un dossier contenant une instance et sa solution.

<h2> Format des fichiers d'instance</h2>

<ul>
<li>instance :</br>

    name domains level techs interv abandon
    myexample 3 2 20 100 1000

</li>
<li>interv_list :</br>

    number time preds prio cost d1_1 d1_2 d2_1 d2_2 d3_1 d3_2
    1 40 [ ] 1 200 0 0 0 0 1 0
    2 12 [ 1 ] 3 350 1 0 0 0 0 0
    3 52 [ 1 2 ] 3 500 1 1 1 0 1 0
    ...
</li>
<li>tech_list :</br>

    tech d1 d2 d3 dispo
    1 0 0 0 [ ]
    2 1 0 2 [ 2 4 ]
    3 2 1 1 [ ]
    ...
</li>
</ul>

<h2> Format des fichiers solutions </h2>

Ces fichiers seront généré par le solver.

<ul>
<li>interv_dates :</br>

    interv day time team
    1 3 55 2
    2 1 27 3
    4 1 0 1
    ...

</li>
<li>tech_teams :</br>

    day team0 team1 team2 team3 ...
    1 [ 3 5 8 ] [ 1 2 7 ] [ 4 ] [ 6 9 ]
    2 [ ] [ 1 5 ] [ 3 ] [ 2 8 ] [ 7 ] [ 4 6 9 ]
    3 [ 2 5 8 ] [ 1 3 7 9 ] [ 4 6 ]
    ...

</li>
</ul>

